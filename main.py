import re
import sys


__author__ = "Zeinab Abbasimazar"


open_tag_pat = re.compile(r"(<\w)>")
close_tag_pat = re.compile(r"(</\w)>")
tag_pat = re.compile(r"(</*[\w/]>)")
tag_char_pat = re.compile(r"</*(\w)>")


def check_tag(t):
    """
    :param t: tag character
    :return: True if opening and closing tags for provided tag character are matched, else False.
    """
    if len(re.findall(open_tag_pat, input_str)) != len(re.findall(close_tag_pat, input_str)):
        sys.stdout.write("Input string is invalid.\n")
    return True


def apply_tags(cur_st):
    """
    :param cur_st: current string to apply tag on
    :return: final processed string
    """
    final_st = cur_st
    # while re.match(tag_pat, final_st):
    #     all_tags = re.findall(tag_pat, final_st)
    #     cur_tag = all_tags[0]
    #     tag_char = re.findall(tag_char_pat, cur_tag)[0]
    #     start_index = cur_st.index(cur_tag)
    #     stack = [cur_tag]
    #     for i, t in enumerate(all_tags[1:]):
    #         if re.match('</(' + tag_char + ')>', t) and len(stack) == 1:
    #             end_index = cur_st.index('</(' + tag_char + ')>')
    #             cur_st = cur_st[start_index:end_index].upper() + cur_st[end_index + 1:]
    #             cur_st = cur_st[start_index + 3:] + cur_st[:end_index - 3]
    #         elif re.match(open_tag_pat, t):
    #             found = re.findall(open_tag_pat, t)[0]
    #             stack.append(found)
    #
    #         index = cur_st.index(found)
    #         cur_st = cur_st[:index] + cur_st[index + 4:]
    #
    #
    #
    # if t == 'l':
    #     for item in r
    # elif t == 'u':
    #
    return final_st


def tags_are_properly_formatted(st):
    """
        :param st: tag character
        :return: True if opening and closing tags for provided tag character are matched, else False.
    """
    tag_stack = list()
    for item in re.findall(tag_pat, st):
        if re.match(open_tag_pat, item):
            tag_stack.append(item)
        elif re.match(close_tag_pat, item):
            ot = tag_stack.pop()
            if re.findall(tag_char_pat, ot) != re.findall(tag_char_pat, item):
                return False
    return len(tag_stack) == 0

if __name__ == "__main__":
    input_str = sys.argv[1]
    sys.stdout.write("Input string as plain text: {0}\n".format(input_str))

    if not re.match(tag_pat, input_str) \
            or len(re.findall(tag_pat, input_str)) % 2 != 0:
        sys.stdout.write("Input string is invalid.\n")
        sys.exit(1)

    if len(re.findall(open_tag_pat, input_str)) != len(re.findall(close_tag_pat, input_str)):
        sys.stdout.write("Input string is invalid.\n")
        sys.exit(1)

    if not tags_are_properly_formatted(input_str):
        sys.stdout.write("Input string is invalid.\n")
        sys.exit(1)

    sys.stdout.write('Input is valid.\n')
    sys.exit(0)
    tag_list = ['l', 'u']
    output_str = input_str
    for t in tag_list:
        if not check_tag(t):
            sys.stdout.write("Input string is invalid.\n")
            sys.exit(1)
        output_str = apply_tag(t, output_str)

    sys.stdout.write("Output string: {0}\n".format(output_str))
